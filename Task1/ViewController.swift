//
//  ViewController.swift
//  Task1
//
//  Created by Artem Tkachev on 1.02.22.
//

import UIKit

class ViewController: UIViewController {
    
    let languages = ["English", "Русский", "Беларускі"]
    
    let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.text = "Hello, User! \nWelcome to Payments."
        label.font = UIFont(name: "Copperplate", size: 17)
        label.tintColor = .black
        label.backgroundColor = .clear
        label.textAlignment = .center
        return label
    }()
    
    let logoImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "blackLogo")
        view.tintColor = .black
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let lightThemeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "lightModeBlack"), for: .normal)
        button.addTarget(self, action: #selector(didTapLightThemeButton), for: .touchUpInside)
        button.backgroundColor = .clear
        return button
    }()
    
    let darkThemeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "nightModeBlack"), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(didTapDarkThemeButton), for: .touchUpInside)
        return button
    }()
    
    let autoThemeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "autoModeBlack"), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(didTapAutoThemeButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemPink
        setConstraints()
        showPicker()
    }
    
    func showPicker() {
        let picker = UIPickerView()
        picker.delegate = self
        
        view.addSubview(picker)
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            picker.heightAnchor.constraint(equalToConstant: 200),
            picker.widthAnchor.constraint(equalToConstant: 200),
            picker.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -40)
        ])
    }
    
    @objc func didTapDarkThemeButton() {
        view.backgroundColor = .systemGray
        logoImageView.image = UIImage(named: "whiteLogo")
        lightThemeButton.setImage(UIImage(named: "lightMode"), for: .normal)
        darkThemeButton.setImage(UIImage(named: "nightMode"), for: .normal)
        autoThemeButton.setImage(UIImage(named: "autoMode"), for: .normal)
        label.textColor = .white
    }
    
    @objc func didTapLightThemeButton() {
        view.backgroundColor = .systemPink
        logoImageView.image = UIImage(named: "blackLogo")
        lightThemeButton.setImage(UIImage(named: "lightModeBlack"), for: .normal)
        darkThemeButton.setImage(UIImage(named: "nightModeBlack"), for: .normal)
        autoThemeButton.setImage(UIImage(named: "autoModeBlack"), for: .normal)
        label.textColor = .black
    }
    
    @objc func didTapAutoThemeButton() {
        let date = NSDate()
        let calendar = Calendar.current
        let currentHour = calendar.component(.hour, from: date as Date)
        
        if currentHour < 19 {
            didTapLightThemeButton()
        } else {
            didTapDarkThemeButton()
        }
    }
    
    private func setConstraints() {
        
        view.addSubview(label)
        view.addSubview(logoImageView)
        view.addSubview(lightThemeButton)
        view.addSubview(darkThemeButton)
        view.addSubview(autoThemeButton)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        lightThemeButton.translatesAutoresizingMaskIntoConstraints = false
        darkThemeButton.translatesAutoresizingMaskIntoConstraints = false
        autoThemeButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            logoImageView.heightAnchor.constraint(equalToConstant: 180),
            logoImageView.widthAnchor.constraint(equalToConstant: 200),
            logoImageView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 150)
        ])
        
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            label.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            lightThemeButton.heightAnchor.constraint(equalToConstant: 30),
            lightThemeButton.widthAnchor.constraint(equalToConstant: 30),
            lightThemeButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            lightThemeButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -770)
        ])
        
        NSLayoutConstraint.activate([
            darkThemeButton.heightAnchor.constraint(equalToConstant: 30),
            darkThemeButton.widthAnchor.constraint(equalToConstant: 30),
            darkThemeButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -60),
            darkThemeButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -770)
        ])
        
        NSLayoutConstraint.activate([
            autoThemeButton.heightAnchor.constraint(equalToConstant: 30),
            autoThemeButton.widthAnchor.constraint(equalToConstant: 30),
            autoThemeButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100),
            autoThemeButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -770)
        ])
    }
}

extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        3
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languages[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            label.text = "Hello, User! \nWelcome to Payments."
        case 1:
            label.text = "Привет, Пользователь! \nДобро пожаловать в Платежи."
        case 2:
            label.text = "Прывітанне, Карыстальнік! \nВітаем у Плацяжах."
        default:
            return
        }
    }

}

